<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarteController extends AbstractController
{
    /**
     * @Route("/", name="carte")
     */
    public function index(ProductRepository $repo): Response
    {   
        $carte = $repo->findAll();
        return $this->render('carte/index.html.twig',[
            "cartes" => $carte
        ]);
    }
}
